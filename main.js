var currentColor = new Color(0,0,0);
var colorDelta = new Color(5, 5, 5);
var winColor = new Color(255, 255, 255);
var loseColor = new Color(0, 0, 0);

var currentScore = 127; //goes from 0 to 100

var won = false;
var lost = false;

//INPUT HANDLING
document.addEventListener('keydown', keyDownHandler, false);
document.addEventListener('keyup', keyUpHandler, false);

var consonantHeld = false;
var consonantDown = false;
var consonantRaised = false;
var vowelHeld = false;
var vowelDown = false;
var vowelRaised = false;

var consonants = ['b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'z'];
var vowels = ['a', 'e', 'i', 'o', 'u'];

function keyDownHandler(event) {
    var keyPressed = event.key.toLowerCase();
    if (consonants.includes(keyPressed)){
	consonantDown = true;
	consonantHeld = true;
    }
    if (vowels.includes(keyPressed)){
	vowelDown = true;
	vowelHeld = true;
    }
}
function keyUpHandler(event) {
    var keyPressed = event.key.toLowerCase();
    if (consonants.includes(keyPressed)){
	consonantHeld = false;
	consonatnRaised = true;
    }
    if (vowels.includes(keyPressed)){
	vowelHeld = false;
	vowelRaised = true;
    }
}

function resetKeys(){
    consonantDown = false;
    consonantRaised = false;
    vowelDown = false;
    vowelRaised = false;
}

;(function() {
    function main( time ) {
	window.requestAnimationFrame( main );

	//main loooop
	if (vowelDown){
	    currentScore += 1;
	}
	if (consonantDown){
	    currentScore -= 1;
	}

	if (!won && !lost){
	    currentScore = Math.max(0, Math.min(currentScore, 255));
	    currentColor = new Color(255 - currentScore, currentScore, 0);
	    console.log(currentScore);
	}

	if (currentScore == 255){
	    Win();
	}
	else if (currentScore == 0){
	    Lose();
	}

	//display logic
	if (won) {
	    currentColor.lerpTowards(winColor, 0.08);
	}
	else if (lost) {
	    currentColor.lerpTowards(loseColor, 0.08);
	}

	//flicker
	var flickerValue = (Math.random() * 40) - 20;
	currentColor.addColor(new Color(flickerValue, flickerValue, flickerValue));
	if (won){
	    flickerValue + 4;
	}
	else if (lost){
	    flickerValue - 4;
	}
	
	setColor(currentColor.toCSS());


	//end of frame
	resetKeys();
    }

    main();
})();

function Win() {
    console.log("won");
    won = true;
    lost = false;
}
function Lose() {
    lost = true;
    won = false;
}

///Color stuff that should be in a different file but doesn't want to
function Color(r, g, b) {
    this.r = r;
    this.g = g;
    this.b = b;

    this.toCSS = toCSS;
    this.addColor = addColor;
    this.lerpTowards = lerpTowards;
}

function toCSS() {
    return ("rgb(" + this.r + "," + this.g + "," + this.b + ")");
}

function addColor(otherColor) {
    this.r += otherColor.r;
    this.g += otherColor.g;
    this.b += otherColor.b;
}

function lerpTowards(targetColor, amount) {
    this.r += (targetColor.r - this.r)*amount;
    this.g += (targetColor.g - this.g)*amount;
    this.b += (targetColor.b - this.b)*amount;
}
///

function setColor(color) {
    document.body.style.backgroundColor = color;
}
